CursorControl-windows
=====================

The desktop cursor control counterpart to the Android application. 
Developed with VS2012 on .NET 4.5. 

Usage:
------

1. Download and install the Android app. 
2. Build and run the application. 
3. Enter the IP address the popup shows into the Android app. 
  * A right click on the tray icon shows more options. 
4. Enjoy your new trackpad!
