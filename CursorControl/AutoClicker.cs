﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CursorControl
{
    class AutoClicker
    {
        public int ClicksPerSecond { get; set; }

        private Cursor _cursor;
        private bool _clicking;
        private Thread _clickerThread;

        public AutoClicker(int clicksPerSecond)
        {
            ClicksPerSecond = clicksPerSecond;
            _cursor = new Cursor();
            _clicking = false;

            _clickerThread = new Thread(ThreadFunction);
            _clickerThread.Start();
        }

        public void ToggleClicking()
        {
            lock (this)
            {
                _clicking = !_clicking;
                Console.WriteLine("Clicking now: {0}", _clicking);
                _clickerThread.Interrupt();
            }
        }

        private void ThreadFunction()
        {
            while (true)
            {
                try
                {
                    while (_clicking)
                    {
                        _cursor.Click(clickSleepMillis: 20);
                        Thread.Sleep(1000 / ClicksPerSecond);
                    }
                    Thread.Sleep(10000);
                }
                catch (ThreadStateException e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                catch (ThreadInterruptedException)
                {
                    // Console.WriteLine(@"Thread interrupted. ");
                    continue;
                }
            }
        }
    }
}
