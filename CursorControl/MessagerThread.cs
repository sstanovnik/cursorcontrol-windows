﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace CursorControl {
    /// <summary>
    /// An instance of this class handles the communication between client devices and this server. 
    /// </summary>
    class MessagerThread {

        private Thread thread;

        /// <summary>
        /// The UDP socket that receives datagrams. 
        /// </summary>
        private UdpClient udpClient;

        /// <summary>
        /// The IP/port combination the socket should listen on. 
        /// </summary>
        private IPEndPoint ipe;

        /// <summary>
        /// A <see cref="Cursor"/> instance to apply messages to. 
        /// </summary>
        private Cursor cursor;

        /// <summary>
        /// A state variable that shows whether the thread is currently running. 
        /// </summary>
        private bool running;

        /// <summary>
        /// Creates a new thread, but does not start it. 
        /// Initializes the socket and cursor. 
        /// </summary>
        internal MessagerThread() {
            // create a new thread, with Run() as the running function
            thread = new Thread(Run);

            udpClient = new UdpClient(31337) { DontFragment = true };
            ipe = new IPEndPoint(IPAddress.Any, 31337);
            cursor = new Cursor();

            running = false;
        }

        /// <summary>
        /// The main function that runs when the thread is started. 
        /// Essentially a message receive-parse loop. 
        /// </summary>
        private void Run() {

            // loop forever, or until stopped
            // the usage of the running variable may be redundant
            while (running) {
                byte[] data = udpClient.Receive(ref ipe);
                Console.Out.WriteLine(Utils.BytesToHexString(data));

                if (data.Length != MessageScheme.MESSAGE_LENGTH) {
                    Console.Out.WriteLine("Invalid data length: {0} bytes. ", data.Length);
                }

                MessageScheme.Action action = MessageScheme.GetAction(data);
                if (action == null) {
                    Console.Out.WriteLine("Invalid action. ");
                    continue;
                }

                switch (action.type) {
                    case MessageScheme.TYPE_MOVE:
                        cursor.Move(action.param1, action.param2);
                        break;
                    case MessageScheme.TYPE_CLICK:
                        cursor.Click();
                        break;
                    case MessageScheme.TYPE_RIGHTCLICK:
                        cursor.RightClick();
                        break;
                }
            }
        }

        /// <summary>
        /// Starts the thread's execution. 
        /// </summary>
        internal void Start() {
            running = true;
            thread.Start();
        }

        /// <summary>
        /// Stops the thread's execution. 
        /// </summary>
        internal void Abort() {
            running = false;
            udpClient.Close();
            thread.Abort();
        }
    }
}
