﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CursorControl
{
    /// <summary>
    /// Values needed for the global keyboard hook. 
    /// </summary>
    class KeyboardHook
    {
        internal const int WH_KEYBOARD_LL = 13;
        internal const int WM_KEYDOWN = 0x0100;

        /// <summary>
        /// The definition of the delegate function that processes the keypress. 
        /// </summary>
        /// <param name="nCode"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        internal delegate void KeyboardCallback(int nCode, IntPtr wParam, IntPtr lParam);

        /// <summary>
        /// The instance of the callback function, passed through SetHook. 
        /// </summary>
        private KeyboardCallback _callback;

        /// <summary>
        /// The ID Windows has given our hook. 
        /// </summary>
        private IntPtr _keyboardHookId;

        public KeyboardHook()
        {
            _keyboardHookId = IntPtr.Zero;

        }

        /// <summary>
        /// Initializes and sets the hook. 
        /// </summary>
        /// <param name="cb">The callback to call on event. </param>
        internal void SetHook(KeyboardCallback cb)
        {
            _callback = cb;

            using (var curProcess = Process.GetCurrentProcess())
            using (var curModule = curProcess.MainModule)
            {
                WinAPI.SetWindowsHookEx(WH_KEYBOARD_LL, HookCallback, WinAPI.GetModuleHandle(curModule.ModuleName), 0);
            }
        }

        /// <summary>
        /// Removes the hook. 
        /// </summary>
        internal void DestroyHook()
        {
            WinAPI.UnhookWindowsHookEx(_keyboardHookId);
        }

        /// <summary>
        /// A wrapper callback for the system call. 
        /// This signature must be passed to a system call as a callback of this format. 
        /// We use a wrapper to obscure away the hook ID. 
        /// </summary>
        /// <param name="nCode"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        internal IntPtr HookCallback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            _callback(nCode, wParam, lParam);
            return WinAPI.CallNextHookEx(_keyboardHookId, nCode, wParam, lParam);
        }
    }
}
