﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CursorControl {
    class Program
    {
        private static AutoClicker Clicker;

        [STAThread]
        public static void Main(string[] args) {
            // create and start a new messager thread to handle communications
            MessagerThread messagerThread = new MessagerThread();
            messagerThread.Start();

            Clicker = new AutoClicker(500);

            // set up and display a tray icon
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var hook = new KeyboardHook();
            hook.SetHook(Callback);

            using (TrayIcon trayIcon = new TrayIcon()) {
                trayIcon.Display();
                Application.Run();
            }

            hook.DestroyHook();

            // when the application (tray icon) exits, this is called to stop the message processing
            messagerThread.Abort();
        }

        private static void Callback(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode >= 0 && wParam == (IntPtr) KeyboardHook.WM_KEYDOWN && ((Keys)Marshal.ReadInt32(lParam)) == Keys.Subtract)
            {
                Clicker.ToggleClicking();
            }
        }
    }
}
