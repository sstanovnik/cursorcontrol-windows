﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Windows.Forms;

namespace CursorControl {
    /// <summary>
    /// An utility class with miscellaneous support functions. 
    /// </summary>
    class Utils {
        /// <summary>
        /// Gets the machine hostname. 
        /// </summary>
        /// <returns>The machine hostname. </returns>
        public static string GetHostName() {
            return Dns.GetHostName();
        }

        /// <summary>
        /// Finds the local IPv4 address that the computer uses on the default local network. 
        /// This is the IP bound to the interface with a default gateway. 
        /// </summary>
        /// <returns>A single local IPv address. </returns>
        public static string GetLocalIP() {
            return NetworkInterface.GetAllNetworkInterfaces()
                        .First(p => p.GetIPProperties().GatewayAddresses.Count != 0)
                        .GetIPProperties()
                        .UnicastAddresses
                        .First(p => p.Address.GetAddressBytes().Length == 4)
                        .Address
                        .ToString();
        }

        /// <summary>
        /// Retrieves an array of all local IP (v4 or v6) addresses bound to this machine. 
        /// </summary>
        /// <returns>An array o IPv4/IPv6 addresses. </returns>
        public static string[] GetAllLocalIPs() {
            // this also removes the ::%mask from IPv6 addresses
            List<string> list = Dns.GetHostEntry(Dns.GetHostName()).AddressList.Select(o => o.ToString().Split('%')[0]).ToList();
            list.Sort((s, s1) => s.Length - s1.Length);
            return list.ToArray();
        }

        /// <summary>
        /// Shows a <see cref="MessageBox"/> with a list of all available local IP addresses, as retrieved by <see cref="GetAllLocalIPs"/>
        /// </summary>
        public static void ShowLocalIPs() {
            string messageContents = "These are all local IP addresses available for your computer: ";
            foreach (string ip in GetAllLocalIPs()) {
                messageContents += "\n" + ip;
            }
            MessageBox.Show(messageContents, "Local IP list", MessageBoxButtons.OK, MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1);
        }

        /// <summary>
        /// Converts a byte array to a hex string. 
        /// https://stackoverflow.com/questions/311165/how-do-you-convert-byte-array-to-hexadecimal-string-and-vice-versa/14333437#14333437
        /// </summary>
        /// <param name="bytes">The byte array to convert to hex. </param>
        /// <returns>A string of hexadecimal characters corresponding to the array contents. </returns>
        protected internal static string BytesToHexString(byte[] bytes) {
            char[] c = new char[bytes.Length * 2];
            int b;
            for (int i = 0; i < bytes.Length; i++) {
                b = bytes[i] >> 4;
                c[i*2] = (char)(55 + b + (((b - 10) >> 31) & -7));
                b = bytes[i] & 0xF;
                c[i*2 + 1] = (char)(55 + b + (((b - 10) >> 31) & -7));
            }
            return new string(c);
        }
    }
}
