﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace CursorControl {
    
    /// <summary>
    /// A representation of a hardware cursor. 
    /// </summary>
    class Cursor {
        /// <summary>
        /// The current position of the cursor. Not automatically updated. 
        /// </summary>
        private Point currentPosition;

        /// <summary>
        /// A struct to pass to a native command for mouse movement. 
        /// </summary>
        private INPUT mouseInput;

        /// <summary>
        /// Initializes a new <see cref="Cursor"/> object. 
        /// </summary>
        public Cursor() {
            InitMouseMessages();
        }
        
        /// <summary>
        /// Moves the cursor <see cref="dx"/> pixels horizontally and <see cref="dy"/> pixels vertically. 
        /// </summary>
        /// <param name="dx">The number of pixels to move horizontally. </param>
        /// <param name="dy">The number of pixels to move vertically. </param>
        protected internal void Move(int dx, int dy) {
            GetPosition();
            Point finalPoint = new Point(currentPosition.x + dx, currentPosition.y + dy);
            Console.Out.WriteLine("Moving mouse {0} -> {1} [{2}, {3}]", currentPosition, finalPoint, dx, dy);

            mouseInput.U.mi.dx = dx;
            mouseInput.U.mi.dy = dy;
            mouseInput.U.mi.dwFlags = MOUSEEVENTF.MOVE;
            SendMouseEvent();
        }

        /// <summary>
        /// Moves the cursor <see cref="dx"/> pixels horizontally and <see cref="dy"/> pixels vertically, 
        /// interpolating motion per pixel with Bresenham's line algorithm: <see cref="InterpolateLine"/>. 
        /// </summary>
        /// <param name="dx">The number of pixels to move horizontally. </param>
        /// <param name="dy">The number of pixels to move vertically. </param>
        [Obsolete("Provides no benefit, probably. ")]
        protected internal void MoveInterpolated(int dx, int dy) {
            GetPosition();
            Point finalPoint = new Point(currentPosition.x + dx, currentPosition.y + dy);
            Console.Out.WriteLine("Moving mouse {0} -> {1} [{2}, {3}]", currentPosition, finalPoint, dx, dy);

            Point previous = currentPosition;
            mouseInput.U.mi.dwFlags = MOUSEEVENTF.MOVE;
            foreach (Point point in InterpolateLine(currentPosition, finalPoint)) {
                mouseInput.U.mi.dx = point.x - previous.x;
                mouseInput.U.mi.dy = point.y - previous.y;
                SendMouseEvent();

                previous = point;
            }
        }

        /// <summary>
        /// Performs a click event at the cursor's current position. 
        /// </summary>
        protected internal void Click(bool output = false, uint clickSleepMillis = 0) {
            GetPosition();
            if (output)
            {
                Console.Out.WriteLine("Clicking @ ({0}, {1})", currentPosition.x, currentPosition.y);
            }

            mouseInput.U.mi.dx = 0;
            mouseInput.U.mi.dy = 0;

            // to perform a click, there have to be two separate events
            mouseInput.U.mi.dwFlags = MOUSEEVENTF.LEFTDOWN;
            SendMouseEvent();

            // optional sleep between these
            if (clickSleepMillis != 0)
            {
                Thread.Sleep((int) clickSleepMillis);
            }

            mouseInput.U.mi.dwFlags = MOUSEEVENTF.LEFTUP;
            SendMouseEvent();
        }

        public void RightClick() {
            GetPosition();
            Console.Out.WriteLine("Right-clicking @ ({0}, {1})", currentPosition.x, currentPosition.y);

            mouseInput.U.mi.dx = 0;
            mouseInput.U.mi.dy = 0;

            mouseInput.U.mi.dwFlags = MOUSEEVENTF.RIGHTDOWN;
            SendMouseEvent();
            mouseInput.U.mi.dwFlags = MOUSEEVENTF.RIGHTUP;
            SendMouseEvent();
        }

        /// <summary>
        /// Initializes the message structure used for passing to the Windows native API. 
        /// See https://msdn.microsoft.com/en-us/library/windows/desktop/ms646273%28v=vs.85%29.aspx for more information. 
        /// </summary>
        private void InitMouseMessages() {
            mouseInput = new INPUT {
                type = (uint) INPUT_TYPE.MOUSE,
                U = new InputUnion()
            };

            mouseInput.U.mi = new MOUSEINPUT {
                dx = 0,
                dy = 0,
                mouseData = 0,
                dwFlags = MOUSEEVENTF.MOVE,
                time = 0,
                dwExtraInfo = (UIntPtr) 0
            };
        }

        /// <summary>
        /// Sends a mouse event. The event has to be configured beforehand by setting values in <see cref="mouseInput"/>. 
        /// </summary>
        private void SendMouseEvent() {
            WinAPI.SendInput(1, ref mouseInput, INPUT.Size);
        }

        /// <summary>
        /// Updates the <see cref="currentPosition"/> variable with the current cursor position. 
        /// </summary>
        /// <returns>The current position. </returns>
        protected internal Point GetPosition() {
            WinAPI.GetCursorPos(out currentPosition);
            return currentPosition;
        }

        /// <summary>
        /// Implements Bresenham's line algorithm. 
        /// https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
        /// </summary>
        /// <param name="start">The starting point. </param>
        /// <param name="end">The end point. </param>
        /// <returns>Yields a collection of <see cref="Point"/>s on a straight line between <see cref="start"/> and <see cref="end"/>. </returns>
        public static IEnumerable<Point> InterpolateLine(Point start, Point end) {
            int x0 = start.x;
            int y0 = start.y;
            int x1 = end.x;
            int y1 = end.y;
            bool steep = Math.Abs(y1 - y0) > Math.Abs(x1 - x0);
            if (steep) {
                int t;
                t = x0; x0 = y0; y0 = t;
                t = x1; x1 = y1; y1 = t;
            }

            int dx = Math.Abs(x1 - x0);
            int dy = Math.Abs(y1 - y0);
            int error = dx / 2;

            Point outPoint = new Point(0, 0);
            int increment = x0 < x1 ? 1 : -1;
            int ystep = y0 < y1 ? 1 : -1;

            while (x0 != x1) {
                if (steep) {
                    outPoint.x = y0;
                    outPoint.y = x0;
                } else {
                    outPoint.x = x0;
                    outPoint.y = y0;
                }
                Console.Out.WriteLine("    intm: {0}", outPoint);
                yield return outPoint;

                error -= dy;
                if (error < 0) {
                    y0 += ystep;
                    error += dx;
                }
                x0 += increment;
            }
        }
    }
}
