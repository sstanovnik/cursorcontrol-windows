﻿using System;

namespace CursorControl {

    /// <summary>
    /// A class that defines the message structure and parameters. 
    /// </summary>
    class MessageScheme {

        /// <summary>
        /// Represents all possible actions that can be passed from a client to the server. 
        /// </summary>
        internal class Action {
            /// <summary>
            /// The action type. 
            /// </summary>
            internal readonly byte type;

            /// <summary>
            /// The first parameter. 
            /// </summary>
            internal readonly short param1;

            /// <summary>
            /// The second parameter. 
            /// </summary>
            internal readonly short param2;
            
            /// <summary>
            /// A constructor for an action without parameters, like a click. 
            /// </summary>
            public Action(byte type) {
                this.type = type;
                this.param1 = 0;
                this.param2 = 0;
            }

            /// <summary>
            /// A two-parameter constructor. 
            /// </summary>
            public Action(byte type, short param1, short param2) {
                this.type = type;
                this.param1 = param1;
                this.param2 = param2;
            }
        }

        /// <summary>
        /// Defines a constant message length. 
        /// </summary>
        public const int MESSAGE_LENGTH = 5;

        /// <summary>
        /// The MOVE action value. 
        /// </summary>
        public const byte TYPE_MOVE  = 0x0;

        /// <summary>
        /// The CLICK action value. 
        /// </summary>
        public const byte TYPE_CLICK = 0x1;

        /// <summary>
        /// The RIGHTCLICK action value. 
        /// </summary>
        public const byte TYPE_RIGHTCLICK = 0x2;

        /// <summary>
        /// An error action value. This should ever be received, as the client should ignore errors. 
        /// </summary>
        public const byte TYPE_ERROR = 0xff;

        /// <summary>
        /// Gets the action from a packet byte array. Also parses parameters. 
        /// </summary>
        /// <param name="data">The data contained in a client message. </param>
        /// <returns>An <see cref="Action"/> object with the corresponding parameters set. </returns>
        internal static Action GetAction(byte[] data) {
            if (data.Length != MESSAGE_LENGTH) {
                return null;
            }

            // the first (0-th) byte represents the action
            byte action = data[0];
            // bytes 1-2 represent the first parameter 
            short param1 = BitConverter.ToInt16(data, 1);
            // bytes 3-4 represent the second parameter. 
            short param2 = BitConverter.ToInt16(data, 3);

            // we can construct an invalid action here - that's okay, the action is checked in the main loop
            return new Action(action, param1, param2);
        }
    }
}
