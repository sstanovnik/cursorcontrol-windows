﻿using System;
using System.Windows.Forms;

namespace CursorControl {
    public partial class AboutWindow : Form {
        public AboutWindow() {
            Console.Out.WriteLine("Creating about. ");
            InitializeComponent();
        }

        private void button1_Click(object sender, System.EventArgs e) {
            Console.Out.WriteLine("Closing about. ");
            Close();
        }
    }
}
