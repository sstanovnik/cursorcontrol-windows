﻿using System;
using System.Windows.Forms;
using CursorControl.Properties;

namespace CursorControl {
    public class TrayIcon : IDisposable {

        public const int ICON_DISPLAY_TIME = 7000; // ms

        private NotifyIcon notifyIcon;

        private ToolStripMenuItem aboutItem;
        private ToolStripMenuItem ipItem;
        private ToolStripMenuItem hostnameItem;
        private ToolStripMenuItem exitItem;
        
        /// <summary>
        /// Creates a new tray icon. 
        /// </summary>
        public TrayIcon() {
            notifyIcon = new NotifyIcon();
        }

        /// <summary>
        /// Performs the necessary setup and displays the icon. 
        /// </summary>
        public void Display() {
            notifyIcon.MouseClick += TrayIconRightClickHandler;
            notifyIcon.MouseDoubleClick += (sender, args) => Utils.ShowLocalIPs();
            notifyIcon.Icon = Resources.mouse16; // 32px also available
            notifyIcon.Text = Utils.GetLocalIP();
            notifyIcon.Visible = true;

            notifyIcon.ContextMenuStrip = CreateContextMenu();
            UpdateContextMenuValues();

            notifyIcon.BalloonTipIcon = ToolTipIcon.Info;
            notifyIcon.BalloonTipTitle = "Local IP";
            notifyIcon.BalloonTipText = "Your computer's local IPv4 address is " + Utils.GetLocalIP();
            notifyIcon.BalloonTipClicked += (sender, args) => Utils.ShowLocalIPs();
            notifyIcon.ShowBalloonTip(ICON_DISPLAY_TIME);
        }

        /// <summary>
        /// Cleanly disposes of the tray icon and all of its resources. 
        /// </summary>
        public void Dispose() {
            aboutItem.Dispose();
            ipItem.Dispose();
            hostnameItem.Dispose();
            exitItem.Dispose();
            notifyIcon.Dispose();
        }

        /// <summary>
        /// Creates a new context menu for the tray icon. 
        /// </summary>
        /// <returns>The new context menu. </returns>
        private ContextMenuStrip CreateContextMenu() {
            ContextMenuStrip menuStrip = new ContextMenuStrip();

            aboutItem = new ToolStripMenuItem();
            aboutItem.Text = "About";
            aboutItem.Image = Resources.info512;
            aboutItem.Click += (sender, args) => {
                AboutWindow aw = new AboutWindow();
                aw.Show();
                Console.Out.WriteLine("click2");
            };

            hostnameItem = new ToolStripMenuItem();
            hostnameItem.Text = "PLACEHOLDER_TEXT";
            hostnameItem.Enabled = false;

            ipItem = new ToolStripMenuItem();
            ipItem.Text = "PLACEHOLDER_TEXT";
            ipItem.Click += (sender, args) => Utils.ShowLocalIPs();

            exitItem = new ToolStripMenuItem();
            exitItem.Text = "Exit";
            exitItem.Image = Resources.exit512;
            exitItem.Click += (sender, args) => {
                Console.Out.WriteLine("Exiting application. ");
                Application.Exit();
            };

            menuStrip.Items.Add(aboutItem);
            menuStrip.Items.Add(new ToolStripSeparator());
            menuStrip.Items.Add(hostnameItem);
            menuStrip.Items.Add(ipItem);
            menuStrip.Items.Add(new ToolStripSeparator());
            menuStrip.Items.Add(exitItem);

            return menuStrip;
        }

        /// <summary>
        /// Updates the text values of the try icon items. 
        /// </summary>
        private void UpdateContextMenuValues() {
            hostnameItem.Text = Utils.GetHostName();
            ipItem.Text = Utils.GetLocalIP();
        }

        /// <summary>
        /// Handles the right click on the tray icon. Updates context menu texts. 
        /// </summary>
        private void TrayIconRightClickHandler(object sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Right) {
                UpdateContextMenuValues();
                Console.Out.WriteLine("itemrightclick");
            } else {
                Console.Out.WriteLine("itemotherclick");
            }
        }
    }
}
